from flask import Flask
app = Flask(__name__)


@app.route("/")
def hello():
    return "Brand New Hello World from Flask in a uWSGI Nginx Docker container with \
     Python 3.6 (from the example template)"


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
